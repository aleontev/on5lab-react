import React, { Component } from 'react';
import axios from 'axios';
import Form from 'react-jsonschema-form';

const initialState = {
  name: '',
  email: '',
  phone: '',
  message: '',
};

class EmailUs extends Component {
  constructor (props) {
    super(props);
    this.state = initialState;
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    axios({
      method: 'post',
      url: '/api/v1/contact_requests',
      responseType: 'stream',
      data: {
        contact_request: {
          name: this.state.name,
          email: this.state.email,
          phone: this.state.phone,
          message: this.state.message,
        },
      },
    })
      .then((response) => {
        this.setState(initialState);
        event.preventDefault();
      })
      .catch(error => console.log(error));
  }

  handleChange(event) {
    this.setState(event.formData);
  }

  render() {
    const schema = {
      title: '',
      liveValidate: true,
      type: 'object',
      required: ['name', 'email', 'message'],
      properties: {
        name: { type: 'string', minHeight: 3 },
        email: { type: 'string', format: 'email' },
        phone: { type: 'string' },
        message: { type: 'string' },
      },
    };
    const uiSchema = {
      name: {
        'ui:options': {
          label: false,
        },
        'ui:placeholder': 'Name',
      },
      email: {
        'ui:options': {
          label: false,
        },
        'ui:placeholder': 'Email',
      },
      phone: {
        'ui:options': {
          label: false,
        },
        'ui:placeholder': 'Phone',
      },
      message: {
        format: 'textarea',
        'ui:options': {
          label: false,
        },
        'ui:widget': 'textarea',
        'ui:placeholder': 'Message',
      },
    };

    const log = type => console.log.bind(console, type);

    return (
      <div>
        <section className="white trans small visible" id="contact">
          <div className="row">
            <div className="col-md-6 offset-md-3">
              <h2 className="text-center">E-mail us</h2>
              <Form
                schema={schema}
                uiSchema={uiSchema}
                formData={this.state}
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                onError={log('errors')}
                className="simple_form new_contact_request"
                id="formEmail"
              >
                <div>
                  <button type="submit" className="form-control">Send</button>
                </div>
              </Form>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default EmailUs;
