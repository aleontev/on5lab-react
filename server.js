const path = require('path');
const express = require('express');

module.exports = {
  app () {
    const app = express();

    app.use('/public', express.static(path.resolve(__dirname, 'public')));
    app.get('*', (_, res) => { res.sendFile(path.resolve(__dirname, 'public', 'index.html')); });

    return app;
  },
};
