import React from 'react';

const HeaderProjects = () => {
  return (
    <section className="dark trans text-center visible">
      <img className="fs faded scale-down-center" alt="Home" src={require('~/assets/images/projects2-min.jpg')} />
      <div className="centered medium">
        <h1>Our Projects</h1>
        <p> We move mountains.</p>
        <a className="icon" href="#created">
          <span className="fa fa-briefcase" />
          <span> See why</span>
        </a>
      </div>
    </section>
  );
};

export default HeaderProjects;
