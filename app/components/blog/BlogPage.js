import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import Posts from './Posts';
import HeaderBlog from './HeaderBlog';

const BlogPage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderBlog />
      </div>
      <Navigation />
      <div className="content">
        {/* <Posts /> */}
      </div>
      <Footer />
    </span>);
};

export default BlogPage;
