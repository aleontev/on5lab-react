import React, { Component } from 'react';
import axios from 'axios';
import Gallery from 'react-grid-gallery';
import '../../assets/scss/partials/portfolio.scss';

const https = require('https');

class Users extends Component {
  constructor(props) {
    super(props);

    this.state = {
      users: [],
    };
  }

  componentDidMount() {
    const agent = new https.Agent({
      rejectUnauthorized: false,
    });
    axios.get('/api/v1/employees.json', { httpsAgent: agent })
      .then((response) => {
        this.setState({ users: response.data.employees });
        const usersImages = _.map(response.data.employees, (value, key) => {
          return {
            src: value.photo.url,
            caption: value.description,
            alt: value.description,
            thumbnail: value.photo.thumb.url,
            thumbnailWidth: 410,
            thumbnailHeight: 410,
            customOverlay: (
              <div className="caption">
                <div className="centered">
                  <h3>{value.full_name}</h3>
                  <p>{value.position}</p>
                </div>
              </div>
            ),
          };
        });
        this.setState({ users: usersImages });
      })
      .catch(error => console.log(error));
  }

  render() {
    const pictures = this.state.users;
    return (
      <section className="portfolio dark trans small" id="employees">
        <div className="row text-center">
          <div className="col-md-8 offset-md-2">
            <h2> DEVELOPERS TEAM </h2>
          </div>
          <div className="col-md-12">
            <Gallery
              images={pictures}
              enableImageSelection={false}
              showImageCount={false}
              backdropClosesModal
            />
          </div>
        </div>
      </section>
    );
  }
}
export default Users;
