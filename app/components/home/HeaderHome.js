import React from 'react';

const HeaderHome = () => {
  return (
    <section className="dark trans text-center visible">
      <img className="fs faded scale-down-center" alt="Home" src={require('~/assets/images/home3-min.jpg')} />
      <div className="centered medium">
        <h1>FiveLabs</h1>
        <p>Professional Web-Development Team</p>
      </div>
    </section>
  );
};

export default HeaderHome;
