import React from 'react';
import Users from './Users';
import Skills from './Skills';
import Agency from './Agency';
import { configureAnchors } from 'react-scrollable-anchor';


const AboutTeam = () => {
  return (
    <div>
      <Agency />
      <Skills />
      <Users />
    </div>
  );
};

export default AboutTeam;
