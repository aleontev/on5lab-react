import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import Info from './Info';
import HeaderHome from './HeaderHome';

const HomePage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderHome />
      </div>
      <Navigation />
      <div className="content">
        <Info />
      </div>
      <Footer />
    </span>);
};

export default HomePage;
