import React from 'react';
import ScrollableAnchor from 'react-scrollable-anchor';


const Skills = () => {
  return (
    <div>
      <ScrollableAnchor key="skills" id="skills">
        <section className="white trans small text-center" id="services">
          <div className="row">
            <div className="col-md-8 offset-md-2">
              <div className="row">
                <div className="col-md-4 service">
                  <h3>
                    <span className="fa fa-laptop" />
                    <span> Smart coding</span>
                  </h3>
                  <span className="small">
                    <b>Web Development</b>
                    {['✓ Ruby on Rails', '✓ Angular.JS',
                    '✓ React.JS', '✓ DB development and optimization',
                    '✓ RESTful API', '✓ SCSS', '✓ HTML5'].map(item => (
                      <p key={item}> {item}</p>
                  ))}
                    <b>Mobile Development</b>
                    <p> ✓ Android and Java</p>
                  </span>
                </div>
                <div className="col-md-4 service">
                  <h3>
                    <span className="fa fa-cloud" />
                    <span> Cloud Hosting</span>
                  </h3>
                  <span className="small">
                    {['✓ AWS', '✓ Docker',
                    '✓ Heroku', '✓ Digital Ocean'].map(item => (
                      <p key={item}> {item}</p>
                  ))}
                  </span>
                </div>
                <div className="col-md-4 service">
                  <h3>
                    <span className="fa fa-bullhorn" />
                    <span> Marketing</span>
                  </h3>
                  <span className="small">
                    <p> ✓ SEO </p>
                    <p> ✓ API's for analytics</p>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </section>
      </ScrollableAnchor>
    </div>
  );
};

export default Skills;
