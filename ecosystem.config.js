module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    // First application
    {
      name      : 'on5lab-react',
      script    : 'app.js',
      env: {
        PORT: 8080
      },
      env_production : {
        NODE_ENV: 'production'
      }
    }
  ],

  /**
   * Deployment section
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : "root",
      host : "onfivelab.com",
      ref  : "origin/master",
      repo : "git@bitbucket.org:aleontev/on5lab-react.git",
      path : "/root/www/on5lab-react",
      "pre-setup" : "apt-get install git",
      "post-deploy" : 'npm install; npm run-script build; pm2 reload ecosystem.config.js --env production'
    },
    dev : {
      user : 'node',
      host : '127.0.0.1',
      ref  : 'origin/master',
      repo : 'git@bitbucket.org:aleontev/on5lab-react.git',
      path : '/var/www/development',
      "post-deploy" : 'npm install && pm2 reload ecosystem.config.js --env dev',
      env  : {
        NODE_ENV: 'dev'
      }
    }
  }
};
