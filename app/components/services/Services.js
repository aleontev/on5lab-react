import React, { Component } from 'react';
import ServiceItem from '~/components/services/ServiceItem';
import ScrollableAnchor from 'react-scrollable-anchor';

class Services extends Component {
  constructor(props) {
    super(props);

    this.state = {
      items: [
        { title: 'UI/UX Design', icon: 'fa-desktop' },
        { title: 'Smart Coding', icon: 'fa-laptop' },
        { title: 'Cloud Hosting', icon: 'fa-cloud' },
        { title: 'Marketing', icon: 'fa-bullhorn' },
        { title: 'Consultancy', icon: 'fa-building-o' },
        { title: 'Support', icon: 'fa-check-square-o' },
      ],
    };
  }
  render() {
    return (
      <div>
        <ScrollableAnchor key="our" id="our">
          <section className="dark trans visible" id="services">
            <div className="row-fluid centered large">
              <div>
                {this.state.items.map((elem, index) => {
                return (
                  <div key={index}>
                    <ServiceItem item={elem} />
                  </div>
              );
})}
              </div>
            </div>
          </section>
        </ScrollableAnchor>
      </div>
    );
  }
}

export default Services;
