import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import AboutTeam from './AboutTeam';
import HeaderAbout from './HeaderAbout';

const AboutPage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderAbout />
      </div>
      <Navigation />
      <div className="content">
        <AboutTeam />
      </div>
      <Footer />
    </span>);
};

export default AboutPage;
