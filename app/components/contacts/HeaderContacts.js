import React from 'react';

const HeaderContacts = () => {
  return (
    <section className="white trans text-center visible">
      <img className="fs faded scale-down-center" alt="Home" src={require('../../assets/images/contact.jpg')} />
      <div className="centered">
        <h1 className="social-icons">
          <a href="https://www.facebook.com/onfivelabs/">
            <span className="fa fa-facebook" />
          </a>
          <a href="mailto:web.alex.leontev@gmail.com">
            <span className="fa fa-envelope" />
          </a>
        </h1>
      </div>
    </section>
  );
};

export default HeaderContacts;
