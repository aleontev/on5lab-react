import React, { Component } from 'react';
import { Icon } from 'react-fa';

class ServiceItem extends Component {
  render() {
    return (
      <div className="col-md-4 service">
        <h3>
          <span className={`fa ${this.props.item.icon}`} />
          <span> {this.props.item.title}</span>
        </h3>
      </div>
    );
  }
}

export default ServiceItem;
