import React from 'react';
import GalleryBlock from './instaGallery/GalleryBlock';
import '~/assets/scss/partials/footer.scss';

const Footer = () => {
  return (
    <section className="dark visible" id="footer">
      <div className="row-fluid">
        <div className="col-md-10 offset-md-1">
          <div className="row">
            <div className="col-md-6 block small" />
            <GalleryBlock />
            <div className="col-md-3 block small">
              <h4>
                                Find Us
              </h4>
              <div className="row">
                <div className="col-md-1 col-xs-2">
                  <div className="fa fa-location-arrow pre-icon" />
                </div>
                <div className="col-md-11 col-xs-9">
                  <p className="small">
                                        Polakovskoy highway 16
                    <br />
                                            Pressmash, ABK-3, №302
                  </p>
                </div>
                <div className="col-md-1 col-xs-2">
                  <div className="fa fa-phone pre-icon" />
                </div>
                <div className="col-md-11 col-xs-9">
                  <p className="small">
                                        +7 (988) 891 6532
                  </p>
                </div>
                <div className="col-md-1 col-xs-2">
                  <div className="fa fa-briefcase pre-icon" />
                </div>
                <div className="col-md-11 col-xs-9">
                  <p className="small">
                                        10.00 - 18.00 GMT+04:00
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="row-fluid foot">
        <div className="col-md-10 offset-md-1">
          <div className="row inner">
            <div className="col-md-12 block small bottom">
              <div className="btn-group btn-group-justified">
                <div className="btn-group">
                  <a className="icon" href="https://www.facebook.com/onfivelabs/">
                    <span className="fa fa-facebook" />
                  </a>
                </div>
                <div className="btn-group">
                  <a className="icon" href="https://www.linkedin.com/company-beta/18025234/">
                    <span className="fa fa-linkedin" />
                  </a>
                </div>
                <div className="btn-group">
                  <a className="icon" href="https://www.instagram.com/explore/tags/on5lab/">
                    <span className="fa fa-instagram" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};
export default Footer;
