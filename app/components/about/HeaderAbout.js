import React from 'react';
import { Link } from 'react-router-dom';

const HeaderAbout = () => {
  return (
    <section className="dark trans text-center visible">
      <img className="fs faded scale-down-center" alt="Home" src={require('~/assets/images/about4-min.jpg')} />
      <div className="centered medium">
        <h1>About Us</h1>
        <p> What's behind The Agency?</p>
        <a className="icon" href="#agency">
          <span className="fa fa-align-left" />
          <span> Read about us</span>
        </a>
      </div>
    </section>
  );
};

export default HeaderAbout;
