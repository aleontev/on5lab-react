import React from 'react';

const HeaderBlog = () => {
  return (
    <section className="dark trans text-center visible">
      <img className="fs faded scale-down-center" alt="Home" src={require('~/assets/images/blog2.jpg')} />
      <div className="centered medium">
        <h1>BLOG</h1>
        <p> Wonderfull blog</p>
      </div>
    </section>
  );
};

export default HeaderBlog;
