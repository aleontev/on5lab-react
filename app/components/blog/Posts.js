import React, { Component } from 'react';
import axios from 'axios';

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      posts: [],
    };
  }

  componentDidMount() {
    axios.get('/api/v1/posts.json')
      .then((response) => {
        this.setState({ posts: response.data.posts });
      })
      .catch(error => console.log(error));
  }
  render() {
    return (
      <div>
        <h1>Posts list</h1>
        <div>
          <table className="table table-bordered table-hover table-inverse">
            <thead className="thead-inverse">
              <tr>
                <th>ID</th>
                <th>Title</th>
                <th>Article</th>
              </tr>
            </thead>
            <tbody>
              {this.state.posts.map((post) => {
                  return (
                    <tr key={post.id}>
                      <td>{post.id}</td>
                      <td>{post.title}</td>
                      <td>{post.article}</td>
                    </tr>);
              })}
            </tbody>
          </table>
        </div>
      </div>

    );
  }
}

export default Projects;
