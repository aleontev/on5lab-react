import React, { Component } from 'react';
import { map, mapValues } from 'lodash';
import axios from 'axios';
import Gallery from 'react-grid-gallery';
import PropTypes from 'prop-types';

const https = require('https');

class GalleryBlock extends Component {
  constructor(props) {
    super(props);

    this.state = {
      photos: [],
    };
  }

  componentDidMount() {
    const agent = new https.Agent({
      rejectUnauthorized: false,
    });
    axios.get('https://www.instagram.com/explore/tags/on5lab/?__a=1', { httpsAgent: agent })
      .then((response) => {
        const findedPhoto = response.data.graphql.hashtag.edge_hashtag_to_media.edges;
        const topPhoto = _.mapValues(_.map(findedPhoto.slice(0, 6), 'node'), (o) => {
          return {
            src: o.display_url,
            thumbnail: o.thumbnail_src,
            thumbnailWidth: 320,
            thumbnailHeight: 320,
          };
        });
        this.setState({ photos: _.values(topPhoto) });
      })
      .catch(error => console.log(error));
  }

  render() {
    return (
      <div className="col-md-3 block small">
        <h4>
          Shots
        </h4>
        <div
          className="zoom-gallery-footer"
        >
          <Gallery images={this.state.photos} enableImageSelection={false} />

        </div>
      </div>
    );
  }
}


export default GalleryBlock;
