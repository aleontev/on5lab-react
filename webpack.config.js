const { resolve } = require('path');

const webpack = require('webpack');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const OpenBrowserPlugin = require('open-browser-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const config = {

  devtool: 'cheap-module-eval-source-map',

  context: resolve(__dirname, 'app'),

  entry: [
    'react-hot-loader/patch',
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server',
    './main.js',
    './assets/scss/main.scss',
  ],

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: resolve(__dirname, 'app'),
        query: {
          plugins: ['transform-runtime'],
        },
      },
      {
        test: /\.scss$/,
        loaders: 'style-loader!css-loader!sass-loader',
        include: resolve(__dirname, 'app', 'assets', 'scss'),
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192',
        include: resolve(__dirname, 'app', 'assets', 'images'),
      },
      {
        test: /\.ico$/,
        loader: 'file-loader?name=[name].[ext]', // <-- retain original file name
      },
      {
        test: /apple-touch-icon\.png$/,
        loader: 'file-loader?name=[name].[ext]', // <-- retain original file name
      },
      {
        test: /\.svg$/,
        loader: 'file-loader',
        include: resolve(__dirname, 'app', 'assets', 'images'),
      },
      {
        test: /\.(woff2?|ttf|eot|svg)(\?[a-z0-9=\.]+)?$/,
        loader: 'url-loader?limit=100000&mimetype=application/font-woff\'',
        include: resolve(__dirname, 'app', 'assets', 'fonts'),
      },
      { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        include: resolve(__dirname, 'node_modules'),
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
    ],
  },

  plugins: [
    new webpack.LoaderOptionsPlugin({
      test: /\.js$/,
      options: {
        eslint: {
          configFile: resolve(__dirname, '.eslintrc'),
          cache: false,
        },
      },
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery',
      Popper: ['popper.js', 'default'],
    }),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new HtmlWebpackPlugin({ template: `${__dirname}/app/index.html`, filename: 'index.html', inject: 'body' }),
    new ExtractTextPlugin({ filename: './styles/style.css', disable: false, allChunks: true }),
    new CopyWebpackPlugin([{ from: 'vendors', to: 'vendors' }]),
    new OpenBrowserPlugin({ url: 'http://localhost:8080' }),
    new webpack.HotModuleReplacementPlugin(),
  ],
};

module.exports = config;
