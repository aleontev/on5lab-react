import React from 'react';
import '~/assets/scss/main.scss';
import '~/assets/scss/partials/about-us.scss';

const Info = () => {
  return (
    <section className="text-center dark" id="about">
      <div className="centered medium">
        <h2>About US</h2>
        <p>We are Ruby on Rails Team based in Taganrog, Russia.
          Our team of 10 includes Ruby on Rails developers and helps customers to successfully
          do business online. We provide different services
          to our clients in web and mobile directions.
        </p>
      </div>
    </section>
  );
};

export default Info;
