// import 'bootstrap/dist/css/bootstrap.css';
// import 'bootstrap/dist/js/bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import React, { Component } from 'react';
import axios from 'axios';
import { configureAnchors } from 'react-scrollable-anchor';
import { Route, BrowserRouter } from 'react-router-dom';
import { API_URL } from './constants';
// Components
import HomePage from '~/components/home/HomePage';
import AboutPage from '~/components/about/AboutPage';
import ProjectsPage from '~/components/projects/ProjectsPage';
import ServicesPage from '~/components/services/ServicesPage';
import BlogPage from '~/components/blog/BlogPage';
import GamePage from '~/components/game/GamePage';
import ContactPage from '~/components/contacts/ContactsPage';

axios.defaults.baseURL = API_URL;

class Root extends Component {
  componentDidMount() {
    configureAnchors({
      offset: -70,
      scrollDuration: 600,
      keepLastAnchorHash: false,
    });
  }
  render() {
    return (
      <BrowserRouter>
        <div className="wrapper-elem">
          <Route path="/" exact component={HomePage} />
          <Route path="/about" component={AboutPage} />
          <Route path="/projects" component={ProjectsPage} />
          <Route path="/services" component={ServicesPage} />
          <Route path="/posts" component={BlogPage} />
          <Route path="/game" component={GamePage} />
          <Route path="/contact_us" component={ContactPage} />
        </div>
      </BrowserRouter>
    );
  }
}

export default Root;

