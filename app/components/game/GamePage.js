import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import Game from './Game';
import HeaderHome from '~/components/home/HeaderHome';

const GamePage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderHome />
      </div>
      <Navigation />
      <div className="content">
        <Game />
      </div>
      <Footer />
    </span>);
};

export default GamePage;
