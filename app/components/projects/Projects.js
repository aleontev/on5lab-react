import React, { Component } from 'react';
import axios from 'axios';
import Gallery from 'react-grid-gallery';
import ScrollableAnchor from 'react-scrollable-anchor';
import '../../assets/scss/partials/portfolio.scss';

class Projects extends Component {
  constructor(props) {
    super(props);
    this.state = {
      projects: [],
    };
  }

  componentDidMount() {
    axios.get('/api/v1/projects.json')
      .then((response) => {
        const projectsImages = _.map(response.data.projects, (value, key) => {
          return {
            src: value.image.url,
            caption: value.name,
            alt: value.name,
            thumbnail: value.image.thumb.url,
            thumbnailWidth: 410,
            thumbnailHeight: 410,
            customOverlay: (
              <div className="caption">
                <div className="centered">
                  <h3>{value.name}</h3>
                  <p>{value.description}</p>
                </div>
              </div>
            ),
          };
        });
        this.setState({ projects: projectsImages });
      })
      .catch(error => console.log(error));
  }
  render() {
    const pictures = this.state.projects;
    return (
      <section className="portfolio dark trans small" id="portfolio">
        <div className="row text-center">
          <div className="col-md-12">
            <div className="block">
              <ScrollableAnchor key="created" id="created">
                <h2> Our work </h2>
              </ScrollableAnchor>
            </div>
          </div>
          <div className="col-md-12">
            <Gallery
              images={pictures}
              enableImageSelection={false}
              showImageCount={false}
              backdropClosesModal
            />
          </div>
        </div>
      </section>
    );
  }
}

export default Projects;
