import React from 'react';
import { Link } from 'react-router-dom';
import ScrollableAnchor from 'react-scrollable-anchor';

const Agency = () => {
  return (
    <div>
      <ScrollableAnchor key="agency" id="agency">
        <section className="dark trans small visible" id="about">
          <div className="row">
            <div className="col-md-6 offset-md-3 text-center">
              <h2>We are The Web Agency</h2>
              <a href="#skills" className="icon" >
                <span className="fa fa-briefcase" />
                <span> What we do</span>
              </a>
            </div>
          </div>
        </section>
      </ScrollableAnchor>
    </div>
  );
};

export default Agency;
