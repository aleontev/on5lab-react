import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import Services from './Services';
import HeaderService from './HeaderServices';

const ServicesPage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderService />
      </div>
      <Navigation />
      <div className="content">
        <Services />
      </div>
      <Footer />
    </span>);
};

export default ServicesPage;
