import React from 'react';
import { Link } from 'react-router-dom';

const HeaderService = () => {
  return (
    <section className="dark trans text-center visible">
      <img className="fs faded scale-down-center" alt="Home" src={require('~/assets/images/services2-min.jpg')} />
      <div className="centered medium">
        <h1>Our Services</h1>
        <p> What we actually do</p>
        <a className="icon" href="#our">
          <span className="fa fa-caret-down" />
          <span> Take a look</span>
        </a>
      </div>
    </section>
  );
};

export default HeaderService;
