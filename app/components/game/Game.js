import React from 'react';
import Board from './Board';

const Game = () => {
  return (
    <div className="game col-md-4 offset-md-4">
      <div className="game-board">
        <Board />
      </div>
    </div>
  );
};

export default Game;
