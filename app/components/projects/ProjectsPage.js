import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import Projects from './Projects';
import HeaderProjects from './HeaderProjects';
import { goToTop } from 'react-scrollable-anchor';

const ProjectsPage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderProjects />
      </div>
      <Navigation />
      <div className="content">
        <Projects />
      </div>
      <Footer />
    </span>);
};

export default ProjectsPage;
