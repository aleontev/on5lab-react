import 'bootstrap/dist/css/bootstrap.css';
import React from 'react';
import { Link, NavLink } from 'react-router-dom';
import { Icon } from 'react-fa';


const Navigation = () => {
  return (
    <nav className="text-center dark visible" id="main">
      <div className="logo">
        <span className="fa fa-code" />
        5Labs
      </div>
      <NavLink to="/contact_us" className="icon pull-right">
        <span className="fa fa-envelope-o" />
      </NavLink>
      <NavLink to="/" className="icon toggle-menu pull-right">
        <span className="fa fa-bars" />
      </NavLink>
      <ul className="nav_box">
        <li><NavLink to="/" exact activeClassName="active" > HOME </NavLink></li>
        <li><NavLink to="/about" activeClassName="active" >ABOUT</NavLink></li>
        <li><NavLink to="/projects" activeClassName="active" >PROJECTS</NavLink></li>
        <li><NavLink to="/services" activeClassName="active" > SERVICES </NavLink></li>
        <li><NavLink to="/posts" activeClassName="active" >BLOG</NavLink></li>
        <li><NavLink to="/game" activeClassName="active" >GAME</NavLink></li>
      </ul>
    </nav>
  );
};

export default Navigation;
