import React from 'react';
import Navigation from '~/components/partials/Navigation';
import Footer from '~/components/partials/Footer';
import EmailUs from './EmailUs';
import HeaderContacts from './HeaderContacts';

const ContactPage = () => {
  return (
    <span>
      <div className="header-class">
        <HeaderContacts />
      </div>
      <Navigation />
      <div className="content">
        <EmailUs />
      </div>
      <Footer />
    </span>);
};

export default ContactPage;
