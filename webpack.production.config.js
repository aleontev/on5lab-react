const { resolve } = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const config = {

  context: resolve(__dirname, 'app'),

  entry: [
    './main.js',
    './assets/scss/main.scss',
  ],

  output: {
    filename: '[name]-[hash].js',
    path: resolve(__dirname, 'public'),
    publicPath: '/public/'
  },

  plugins: [
    new webpack.optimize.ModuleConcatenationPlugin(),
    new HtmlWebpackPlugin({ template: `${__dirname}/app/index.html`, filename: 'index.html', inject: 'body' }),
    new webpack.optimize.UglifyJsPlugin(),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new webpack.LoaderOptionsPlugin({ minimize: true, debug: true }),
    new webpack.DefinePlugin({ 'process.env': { NODE_ENV: JSON.stringify('production') } }),
    new CopyWebpackPlugin([{ from: 'vendors', to: 'vendors' }])
  ],

  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        loader: 'babel-loader',
        include: resolve(__dirname, 'app')
      },
      {
        test: /\.scss$/,
        loaders: 'style-loader!css-loader!sass-loader',
        include: resolve(__dirname, 'app', 'assets', 'scss')
      },
      {
        test: /\.(png|jpg)$/,
        loader: 'url-loader?limit=8192',
        include: resolve(__dirname, 'app', 'assets', 'images')
      },
      {
        test: /\.ico$/,
        loader: 'file-loader?name=[name].[ext]', // <-- retain original file name
      },
      {
        test: /apple-touch-icon\.png$/,
        loader: 'file-loader?name=[name].[ext]', // <-- retain original file name
      },
      {
        test: /\.svg$/,
        loader: 'file-loader',
        include: resolve(__dirname, 'app', 'assets', 'images')
      },
      {
        test: /\.(woff2?|ttf|eot|svg)(\?[a-z0-9=\.]+)?$/,
        loader: 'url-loader?limit=10000',
        include: resolve(__dirname, 'app', 'assets', 'fonts')
      },
      { test: /\.(png|woff|woff2|eot|ttf|svg)$/, loader: 'url-loader?limit=100000' },
      {
        test: /\.css$/,
        loader: 'style-loader!css-loader',
        include: resolve(__dirname, 'node_modules'),
      },
      {
        test: /\.json$/,
        loader: 'json-loader',
      },
    ],
  },
};

module.exports = config;
